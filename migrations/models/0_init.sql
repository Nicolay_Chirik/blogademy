-- upgrade --
CREATE TABLE IF NOT EXISTS "user" (
    "id" UUID NOT NULL  PRIMARY KEY,
    "full_name" TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS "collection" (
    "id" UUID NOT NULL  PRIMARY KEY,
    "title" TEXT NOT NULL,
    "position" INT NOT NULL,
    "author_id" UUID NOT NULL REFERENCES "user" ("id") ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS "collectionitem" (
    "id" UUID NOT NULL  PRIMARY KEY,
    "title" TEXT NOT NULL,
    "position" INT NOT NULL,
    "collection_id" UUID NOT NULL REFERENCES "collection" ("id") ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS "aerich" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "version" VARCHAR(255) NOT NULL,
    "app" VARCHAR(20) NOT NULL,
    "content" JSONB NOT NULL
);
