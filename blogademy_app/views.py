from django.http.response import JsonResponse
from django.views.generic import View

from blogademy_app.models import CollectionItem


class TestAPIView(View):
    def get(self, request, *args, **kwargs):
        items = CollectionItem.all()

        return JsonResponse([
            {"id": str(item.id), "position": item.position, "collection_id": str(item.collection_id)} for item in items
        ])