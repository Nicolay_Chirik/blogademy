from models import CollectionItem
from starlette.applications import Starlette
from starlette.requests import Request
from starlette.responses import JSONResponse
from uvicorn.main import run

from tortoise.contrib.starlette import register_tortoise


app = Starlette()


@app.route("/api/test", methods=["GET"])
async def list_all(_: Request) -> JSONResponse:
    items = await CollectionItem.all()
    return JSONResponse([
        {"id": str(item.id), "position": item.position, "collection_id": str(item.collection_id)} for item in items
    ])


TORTOISE_ORM = {
    "connections": {
        "default": "postgres://blogademy:blogademy@db:5432/blogademy_db"
    },
    "apps": {
        "models": {
            "models": ["models"],
            "default_connection": "default",
        },
    },
}
register_tortoise(app, config=TORTOISE_ORM)


if __name__ == "__main__":
    run(app, host="0.0.0.0", port=80)
