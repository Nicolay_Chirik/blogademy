from tortoise.models import Model
from tortoise import fields


class User(Model):
    id = fields.UUIDField(pk=True, unique=True, null=False)
    full_name = fields.TextField()


class Collection(Model):
    id = fields.UUIDField(pk=True, unique=True, null=False)
    title = fields.TextField()
    author = fields.ForeignKeyField('models.User', related_name='author')
    position = fields.IntField()


class CollectionItem(Model):
    id = fields.UUIDField(pk=True, unique=True, null=False)
    collection = fields.ForeignKeyField('models.Collection', related_name='collection')
    title = fields.TextField()
    position = fields.IntField()
